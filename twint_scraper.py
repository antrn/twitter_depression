import pandas as pd
import numpy as np
np.random.seed(10)
import twint
import nest_asyncio
import time
from tqdm import tqdm

nest_asyncio.apply()
depression_df = pd.read_csv('C:/Users/an_tr/Desktop/depressed_group.csv', parse_dates=["date"])
depression_df = depression_df.drop_duplicates(subset='username', keep="last").reset_index(drop=True)



# current_date = datetime(2019,1,31)
# end_date = datetime(2010,10,31)


for i in tqdm(depression_df.index):
    username = depression_df.at[i, "username"]
    timestamp = depression_df.at[i, "date"]

    try:
        c = twint.Config()
        c.Limit = None
        c.Lang = "en"
        c.User_full = True
        c.Profile_full = True
        c.Store_csv = True
        c.Year = timestamp.strftime("%Y-%m-%d")
        c.Username = username
        c.Output = "c:/Users/an_tr/Desktop/depressed_final.csv"

        twint.run.Search(c)
    except Exception as e:
        print(e)
        time.sleep(60 * 2)
        pass
