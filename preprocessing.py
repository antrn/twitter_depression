import pandas as pd
import re
import string
import preprocessor as p
import nltk
from nltk import word_tokenize
import numpy as np
import os
import contractions
from nltk.corpus import stopwords, words
from nltk.tokenize.casual import TweetTokenizer
from tqdm import tqdm
np.random.seed(10)


# Happy Emoticons
# emoticons_happy = {':-)', ':)', ';)', ':o)', ':]', ':3', ':c)', ':>', '=]', '8)', '=)', ':}', ':^)', ':-D', ':D', '8-D',
#                    '8D', 'x-D', 'xD', 'X-D', 'XD', '=-D', '=D', '=-3', '=3', ':-))', ":'-)", ":')", ':*', ':^*', '>:P',
#                    ':-P', ':P', 'X-P', 'x-p', 'xp', 'XP', ':-p', ':p', '=p', ':-b', ':b', '>:)', '>;)', '>:-)', '<3'}

# # Sad Emoticons
# emoticons_sad = {':L', ':-/', '>:/', ':S', '>:[', ':@', ':-(', ':[', ':-||', '=L', ':<', ':-[', ':-<', '=\\', '=/',
#                  '>:(', ':(', '>.<', ":'-(", ":'(", ':\\', ':-c', ':c', ':{', '>:\\', ';('}

# emoticons = emoticons_happy.union(emoticons_sad)

# Emoji patterns
# emoji_pattern = re.compile("["
#                            u"\U0001F600-\U0001F64F"  # emoticons
#                            u"\U0001F300-\U0001F5FF"  # symbols & pictographs
#                            u"\U0001F680-\U0001F6FF"  # transport & map symbols
#                            u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
#                            u"\U00002702-\U000027B0"
#                            u"\U000024C2-\U0001F251"
#                            "]+", flags=re.UNICODE)

def clean_tweets(tweet):
    """
    preprocessing pipeline: data cleansing/standardization + tokenization

    preprocessing pipeline, removing: hashtags, URLs, and mentions/replies/retweets from the tweet

    Parameter:
    arg1 (str): raw tweet

    Returns:
    str: cleaned and tokenized tweet 
    """
    # remove hashtag sign: #
    tweet = re.sub(r'#', '', tweet, flags=re.MULTILINE)

    # remove URLs
    tweet = re.sub(r"http\S+", "", tweet, flags=re.MULTILINE)
    tweet = re.sub(r"www\S+", "", tweet, flags=re.MULTILINE)
    tweet = re.sub(r"pic.\S+", "", tweet, flags=re.MULTILINE)

    # remove replies, mentions, retweets, prefixed by @
    tweet = re.sub(r'@\S+', '', tweet)

    # tokenize
    tweet_tokenizer = TweetTokenizer(strip_handles=True, reduce_len=True)
    word_tokens = tweet_tokenizer.tokenize(tweet)

    filtered_tweet = []
    for word in word_tokens:
        if word not in ['nan']: # and word not in string.punctuation
            filtered_tweet.append(word)
    
    return ' '.join(filtered_tweet)


cwd = os.getcwd()
depression_df = pd.read_csv('C:/Users/an_tr/Desktop/manual_corpus/depressed_corpus_allyears.csv',
                            parse_dates=["date"])
depression_df = depression_df.drop_duplicates(subset='tweet', keep="first").reset_index(drop=True)
no_depression_df = pd.read_csv("{}/{}/{}.csv".format('C:/Users/an_tr/Desktop/twitter_depression', "dataset", "cleaned_non_depressed_corpus"),
                               parse_dates=["date"])
no_depression_df = no_depression_df.drop_duplicates(subset='tweet', keep="first").reset_index(drop=True)

"""
train_random = pd.read_csv('C:/Users/an_tr/Desktop/twitter_depression/dataset/training.1600000.processed.noemoticon.csv', encoding = "ISO-8859-1", usecols=[0, 5], header=None, names=['sentiment', 'text'])
test_random = pd.read_csv('C:/Users/an_tr/Desktop/twitter_depression/dataset/testdata.manual.2009.06.14.csv',encoding = "ISO-8859-1", usecols=[0, 5] , header=None, names=['sentiment', 'text'])
random_df = pd.concat([train_random, test_random], axis=0, ignore_index=True)
"""
subdirectory = '{}/{}/{}'.format(cwd, 'dataset', 'cleaned_datasets')

if not os.path.exists(subdirectory):
    os.makedirs(subdirectory)

depression = pd.DataFrame({'user_id': depression_df.user_id, 'tweet': depression_df.tweet, 'date': depression_df.date})
depression['is_depressed'] = depression.apply(lambda x: 1, axis=1)
depression = depression[depression["tweet"].astype(str) != 'nan']

no_depression = pd.DataFrame(
    {'user_id': no_depression_df.user_id, 'tweet': no_depression_df.tweet, 'date': no_depression_df.date})
no_depression['is_depressed'] = no_depression.apply(lambda x: 0, axis=1)
no_depression = no_depression[no_depression["tweet"].astype(str) != 'nan']
# random = pd.DataFrame({'tweet': non_depression_df.text})
# # random['is_depressed'] = random.apply(lambda x: 0, axis=1)

max_length = max(len(depression), len(no_depression))
max_df = depression if len(depression) == max_length else no_depression
min_df = no_depression if len(depression) == max_length else depression

remove_n = max_length - len(min_df)
drop_indices = np.random.choice(max_df.index, remove_n, replace=False)
max_df = max_df.drop(drop_indices)

print('final lengths: max:', len(max_df), '; min:', len(min_df))

# combine datasets and drop duplicates
combined_df = pd.concat([min_df, max_df])
combined_df = combined_df.sample(frac=1).reset_index(drop=True)
combined_df.dropna(inplace=True)

cols = ['user_id', 'tweet', 'date', 'is_depressed']
rows = []

print('clean dataset...')
for i in tqdm(combined_df.index):
    tmp_clean = clean_tweets(combined_df.at[i, 'tweet'])
    tmp_clean = p.clean(tmp_clean)
    if(len(tmp_clean) > 1): 
        rows.append([combined_df.at[i, 'user_id'], tmp_clean, combined_df.at[i, 'date'], combined_df.at[i, 'is_depressed']])
print('cleaned dataset...')

print('save to file...')
clean_df = pd.DataFrame(rows, columns=cols)
clean_df.to_csv("{}/{}.csv".format('C:\\Users\\an_tr\\Desktop\\twitter_depression', 'corpus_no_processing_standford'), index=None, header=True)
print('saved to file...', clean_df.tail(4))

