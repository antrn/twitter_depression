import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import *
from tensorflow.keras import Sequential
from tensorflow.keras import optimizers
from tensorflow.keras.regularizers import *
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import os, collections
import matplotlib.font_manager as fm
from mlxtend.plotting import plot_confusion_matrix
from tensorflow.keras import backend as K
from matplotlib import colors
import pickle
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, accuracy_score
from gensim.models import KeyedVectors
import matplotlib.font_manager as fm
from hyperopt import Trials, STATUS_OK, tpe
from tensorflow.keras import backend as K
from hyperas import optim
from hyperas.distributions import choice, uniform
import gc
from tensorflow.keras.models import model_from_json
from sklearn.decomposition import PCA 
from matplotlib import rcParams
from tensorflow.keras.constraints import max_norm

rcParams.update({'figure.autolayout': True}) 

MEMORY_LIMIT = 1024*2
CWD = os.getcwd()
DATASET_DIR = "{}/{}".format(CWD, "dataset")
CLEAN_DATASET_DIR = "{}/{}".format(DATASET_DIR, "cleaned_datasets")
TARGET_DIR = "C:/Users/an_tr/Desktop/results/bidirectional/e2v"
EMBEDDING_DIM = 300
MAX_LENGTH = 280
TRUNC_TYPE = PADDING_TYPE = 'post'
OOV_TOKEN = "<OOV>"
BATCH_SIZE = 800
NUM_EPOCHS = 100
LEARNING_RATE = 0.001
THRESHOLD = 0.52

print('GPU available:', tf.test.is_gpu_available(
    cuda_only=False,
    min_cuda_compute_capability=None
))

gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus: 
  try:
    tf.config.experimental.set_virtual_device_configuration(
        gpus[0],
        [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=MEMORY_LIMIT)]) # Restrict TensorFlow to only allocate 1GB of memory on the GPU
    logical_gpus = tf.config.experimental.list_logical_devices("GPU")
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs available...")
  except RuntimeError as e:
    print(e)


def get_predictions(model, test_sentences, threshold=0.5) -> np.array:
    """
    sets the threshold for accuracy

    predicts depressed for values above the threshold, and non-depressed otherwise

    Parameters:
    arg1 (keras.Sequential): is the fitted/trained model
    arg2 (list): tweets in the test set
    arg2 (float): the threshold value, default: 0.5

    Returns:
    np.array: contains the predictions - target value
    """
    predictions = model.predict(test_sentences)
    predictions = predictions > threshold

    return predictions.astype("float64")


'''
create plot of all word vectors and corresponding labels 
within the word embedding layer in a 2D scatter plot
'''
def get_embeddings(model, tokenizer):
    pca = PCA(n_components=2)
    result = pca.fit_transform(model)
    print(result)

    words = result.keys()
    print(words)
    try:
        embeddings = {w:model[i] for w, i in result.items()}
        print(embeddings)
    except Exception as e:
        print(e)
    # words = tokenizer.word_index

    # word_vectors = words = []
    # try:
    #     for word in tqdm(words):
    #         try:
               
    #             index = result[word]
    #             x.append(result[index, 0])
    #             y.append(result[index, 1])

    #             keys.append(key)
    #             word_vectors.append(key)
    #         except Exception as e:     
    #             print('Error:', e)
    #             pass
    #     return keys, word_vectors
    #     pio.templates.default = "plotly_white"
    #     df = pd.DataFrame({'x': np.array(x), 'y': np.array(y), 'labels': np.array(keys)}) 
        
    #     fig = px.scatter(x=df.x, y=df.y, text=df.labels, size_max=80)
    #     fig.update_traces(textposition='top center', marker=dict(color="#EF7F79")) # textfont=dict(size=8)
    #     fig.update_layout(
    #         # height=600,
    #         title = {
    #             'text': 'word embeddings',
    #             'xanchor': 'center',
    #             'yanchor': 'top'},
    #         font=dict(
    #             family="Segoe UI Emoji, Microsoft YaHei UI, MS UI Gothic, Yu Gothic UI, Helvetica",
    #             size=12,
    #             color="#000000"
    #         )
    #     )
        
    #     fig.show()


def create_checkpoint(filepath) -> ModelCheckpoint:
    """
    callback function, saving the best DNN model

    This function tracks the model"s performance over each epoch, and saves the best model to a local file

    Parameters:
    arg1 (str): is the filepath , where the model will be saved to

    Returns:
    ModelCheckpoint: checkpoint of the model, monitoring the best version
    """
    if not os.path.exists("./checkpoints/"):
        os.makedirs("./checkpoints/")

    return ModelCheckpoint(filepath, monitor="val_accuracy", verbose=1, save_best_only=True, mode="max")


def plot_graphs(history, metric):
    """
    plot the train and validation results on the performance metrics

    Parameters:
    arg1 (history object): contains the model results
    arg2 (str): defines the metric, which should be plotted
    arg2 (bool): whether validation results should be included - default: True
    """
    try:
        prop = fm.FontProperties(fname="C:/Users/an_tr/AppData/Local/Microsoft/Windows/Fonts/HelveticaLt.ttf")

        plt.plot(history.history[metric], color="#B8315F", linewidth=1)
        plt.plot(history.history["val_"+metric], color="#FC9C9A", linewidth=1)
        plt.legend([metric, "val_"+metric], prop={'size':18})

        plt.style.use("seaborn-whitegrid")
        plt.xlabel("epochs", fontproperties=prop, fontsize=22)
        plt.ylabel(metric, fontproperties=prop, fontsize=22)
        
        plt.xticks(fontsize=18, fontproperties=prop)
        plt.yticks(fontsize=18, fontproperties=prop)
        plt.grid(linestyle="dotted")
        ax = plt.gca()
        ax.spines["top"].set_visible(False)
        ax.spines["right"].set_visible(False)
        ax.yaxis.grid(True)
        ax.xaxis.grid(False)
        ax.tick_params(axis='both', which='major', labelsize=20)
        plt.savefig("{}/{}.eps".format(TARGET_DIR, metric), format="eps", dpi=380)
        plt.show()

    except Exception as e:
        print("Error- plotting metric: ", e)


def show_confusion_matrix(test_sentences, predictions, test_labels, normalize=True):
    try:
        # predictions = model.predict(test_sentences) #, verbose=1
        print('predictions:', predictions)

        classes = ['non-depressed', 'depressed']
        cm = confusion_matrix(test_labels, predictions)
        tn, fp, fn, tp = cm.ravel()
        wrong_predicted = []
        wrong = []
        for i in range(len(test_labels)):
            if test_labels[i] != predictions[i]:
                wrong_predicted.append(test_sentences)
                error_str = "pred{}-true{}".format(predictions[i], test_labels[i])
                wrong.append(error_str)

        df = pd.DataFrame({'tweet': wrong_predicted, 'error': wrong})
        df.to_csv("{}/{}.csv".format(TARGET_DIR, 'wrong_predictions'), index=None, header=True)
        print('true pos:', tn, '; false pos:', fp, '; false neg:', fn, '; true pos:', tp) 

        cmap = colors.ListedColormap([
        '#FC9C9A', '#EF7F79', '#D9606F', 
        '#B8315F', '#821944', '#6C0F37'
        ])

        _, _ = plot_confusion_matrix(conf_mat=cm,
                                show_absolute=True,
                                show_normed=normalize,
                                colorbar=True,
                                cmap=cmap,
                                class_names=classes)

        img = plt.gcf()
        plt.show()
        img.savefig('{}/cm.svg'.format(TARGET_DIR), dpi=380)
    except Exception as e:     
        print('Error confusion matrix:', e)


    
def recall(y_true, y_pred) -> float:
    """
    Callback function to calculate the performance metric: recall.

    Parameters:
    arg1 (list): actual class labels
    arg2 (list): predicted class labels

    Returns:
    float: recall value
    """
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())

    return recall


def precision(y_true, y_pred) -> float:
    """
    Callback function to calculate the performance metric: precision.

    Parameters:
    arg1 (list): actual class labels
    arg2 (list): predicted class labels

    Returns:
    float: precision value
    """

    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())

    return precision


def f1score(y_true, y_pred) -> float:
    """
    Callback function to calculate the performance metric: f1-score.

    Parameters:
    arg1 (list): actual class labels
    arg2 (list): predicted class labels

    Returns:
    float: f1_score value
    """

    prec = precision(y_true, y_pred)
    rec = recall(y_true, y_pred)
    return 2 * ((prec * rec) / (prec + rec + K.epsilon()))

# print('read in data...')
# with open('{}/{}.pickle'.format(DATASET_DIR,'sentences'), 'rb') as f:
#     sentences = np.array(pickle.load(f))
    
# with open('{}/{}.pickle'.format(DATASET_DIR,'labels'), 'rb') as f:
#     labels = np.array(pickle.load(f))
# print('finished reading...')

print("read in twitter corpus...")
filename = "corpus_2020_03_02"
depression_df = pd.read_csv("{}/{}.csv".format("C:\\Users\\an_tr\\Desktop", filename), parse_dates=["date"])
print("completed...")

sentences = []
labels = []
for i in tqdm(depression_df.index):
    sentences.append(depression_df.at[i, "tweet"])
    labels.append(depression_df.at[i, "is_depressed"])

sentences = np.array(sentences)
labels = np.array(labels)

print('tokenize...')
tokenizer = Tokenizer(oov_token=OOV_TOKEN, lower=False)
tokenizer.fit_on_texts(sentences)

word_index = tokenizer.word_index
vocab_size = len(word_index)
sequences = tokenizer.texts_to_sequences(sentences)
padded = pad_sequences(sequences, maxlen=MAX_LENGTH, padding=PADDING_TYPE, truncating=TRUNC_TYPE)
print('tokenized...')

training_sentences, validation_sentences, training_labels, validation_labels = train_test_split(padded, labels,
                                                                                                test_size=0.4, shuffle=True,
                                                                                                random_state=42, stratify=labels)
validation_sentences, test_sentences, validation_labels, test_labels = train_test_split(validation_sentences,
                                                                                        validation_labels, stratify=validation_labels,
                                                                                        test_size=0.5, shuffle=True, random_state=42)




subdirectory = 'C:\\Users\\an_tr\\Desktop\\pretrained_embeddings'
print("load e2v...")
oov_words = []

emoji2vec_model = KeyedVectors.load_word2vec_format(
    "{}\\{}.bin".format(subdirectory ,'emoji2vec'),
    binary=True,
    unicode_errors="ignore"
)

emoji2vec_embeddings_matrix = np.zeros((vocab_size+1, EMBEDDING_DIM))  
for word, i in tqdm(word_index.items()):
    try:
        embedding_vector = emoji2vec_model[word]        
    except KeyError:
        oov_words.append(word)
        embedding_vector = np.zeros((EMBEDDING_DIM,))
    if embedding_vector is not None:
        emoji2vec_embeddings_matrix[i] = embedding_vector

print("load w2v...")
oov_words = []

word2vec_model = KeyedVectors.load_word2vec_format(
    "{}\\{}.bin".format(subdirectory ,'GoogleNews-vectors-negative300'),
    binary=True,
    unicode_errors="ignore"
)

word2vec_embeddings_matrix = np.zeros((vocab_size+1, EMBEDDING_DIM))  
for word, i in tqdm(word_index.items()):
    try:
        embedding_vector = word2vec_model[word]        
    except KeyError:
        oov_words.append(word)
        embedding_vector = np.zeros((EMBEDDING_DIM,))

    if embedding_vector is not None:
        word2vec_embeddings_matrix[i] = embedding_vector

sequence_input = keras.Input(shape=(MAX_LENGTH,), dtype='int32')
w2v_embedded_inputs = Embedding(vocab_size+1, EMBEDDING_DIM, input_length=MAX_LENGTH, weights=[word2vec_embeddings_matrix], trainable=False)(sequence_input)
e2v_embedded_inputs = Embedding(vocab_size+1, EMBEDDING_DIM, input_length=MAX_LENGTH, weights=[emoji2vec_embeddings_matrix], trainable=False)(sequence_input)

merged = average([w2v_embedded_inputs, e2v_embedded_inputs])
merged = Dropout(0.5)(merged)
conv = Conv1D(filters=16, kernel_size=32, activation='relu')(merged)
conv = MaxPooling1D(7)(conv)
conv = Dropout(0.4)(conv)
gru = Bidirectional(LSTM(128, kernel_regularizer=l2(0.69), recurrent_regularizer=l2(0.81), bias_regularizer=l2(0.25)))(merged)
gru = Dropout(0.1)(gru)
output = Dense(1, activation='sigmoid')(gru)
model = keras.Model(inputs=[sequence_input], outputs=output)

# model = Sequential([
#     Embedding(vocab_size+1, EMBEDDING_DIM, input_length=MAX_LENGTH, weights=[word2vec_embeddings_matrix], trainable=False),
#     GRU(64, recurrent_regularizer=l2(0.5), kernel_regularizer=l2(0.8), bias_regularizer=l2(0.4),return_sequences=True),
#     Dropout(0.1),
#     Flatten(),
#     Dense(1, activation='sigmoid')
# ])

# model = Sequential([
#     Embedding(vocab_size+1, EMBEDDING_DIM, input_length=MAX_LENGTH),
#     # Embedding(vocab_size+1, EMBEDDING_DIM, input_length=MAX_LENGTH, weights=[word2vec_embeddings_matrix], trainable=False),
#     Dropout(0.5),
#     Conv1D(filters=16, kernel_size=32, activation='relu', kernel_constraint=max_norm(3)),
#     MaxPooling1D(7), 
#     Dropout(0.4),
#     Bidirectional(GRU(128, kernel_regularizer=l2(0.69), recurrent_regularizer=l2(0.81), bias_regularizer=l2(0.25), kernel_constraint=max_norm(3), recurrent_constraint=max_norm(3), bias_constraint=max_norm(3))),  
#     Dropout(0.1),
#     # Flatten(),
#     Dense(1, activation='sigmoid')
# ])


adam = optimizers.Adam(LEARNING_RATE)

callbacks = [
    EarlyStopping(monitor='val_loss', min_delta= 0.0001, mode='min', verbose=1, patience=20, restore_best_weights=True)
    ]

model.compile(loss="binary_crossentropy",
              optimizer=adam,
              metrics=["accuracy", precision, recall, f1score])

model.summary()

history = model.fit(training_sentences, training_labels,
                    epochs=NUM_EPOCHS,
                    validation_data=(validation_sentences, validation_labels),
                    batch_size=BATCH_SIZE,
                    callbacks=callbacks
                    )



model.save(TARGET_DIR+'/best_model.h5')

y_pred = get_predictions(model, test_sentences)
m_acc = accuracy_score(test_labels, y_pred)
m_prec = precision_score(test_labels, y_pred)
m_rec = recall_score(test_labels, y_pred)
m_f1_score = f1_score(test_labels, y_pred)

measurements = {
    'accuracy': m_acc,
    'precision': m_prec,
    'recall': m_rec,
    'f1_score': m_f1_score
}

print('acc', m_acc, '; precision:', m_prec, '; recall:', m_rec, '; f1-score:', m_f1_score)
with open('{}/measurements.pickle'.format(TARGET_DIR), 'wb') as handle:
    pickle.dump(measurements, handle, protocol=4)

show_confusion_matrix(test_sentences, y_pred, test_labels)

with open("{}/{}.pickle".format(TARGET_DIR,'history'), 'wb') as f:
    pickle.dump(history.history, f, protocol=4)


plot_graphs(history, 'accuracy')
plot_graphs(history, 'precision')
plot_graphs(history, 'recall')
plot_graphs(history, 'f1score')
plot_graphs(history, 'loss')

word_embeddings = model.layers[0].get_weights()[0]
get_embeddings(word_embeddings, tokenizer)

# print('save word embeddings to file...')
# with open("{}/{}.tsv".format('C:/Users/an_tr/Desktop', 'embeddings'), 'w') as f:
#     try:
#         np.savetxt(f,model.layers[0].get_weights()[0] ,  delimiter='\t')
#     except Exception as e:
#         print('Error - saving embeddings to file:', e)
# print('saved to file...')

del model
K.clear_session()